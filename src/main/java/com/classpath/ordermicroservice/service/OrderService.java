package com.classpath.ordermicroservice.service;

import com.classpath.ordermicroservice.event.OrderEvent;
import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.model.OrderStatus;
import com.classpath.ordermicroservice.repository.OrderRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.TemporalUnit;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static java.time.temporal.ChronoUnit.SECONDS;
import static org.springframework.data.domain.Sort.Direction.ASC;
import static org.springframework.data.domain.Sort.Direction.DESC;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private final OrderRepository orderRepository;
    private final Source source;

    public Map<String, Object> fetchOrders(int page, int size, String strDirection, String property) {
        Sort.Direction direction = strDirection.equalsIgnoreCase("asc") ? ASC : DESC ;
        PageRequest pageRequest = PageRequest.of(page, size, direction, property);
        Page<Order> pageResponse = this.orderRepository.findAll(pageRequest);
        LinkedHashMap<String, Object> responseMap = new LinkedHashMap<>();
        responseMap.put("total-records", pageResponse.getTotalElements());
        responseMap.put("total-pages", pageResponse.getTotalPages());
        responseMap.put("current", pageResponse.getNumber());
        responseMap.put("records", pageResponse.getSize());
        responseMap.put("data", pageResponse.getContent());

        return responseMap;

    }

    public Order fetchOrderById(long orderId){
        return this.orderRepository.findById(orderId)
                            .orElseThrow(() -> new IllegalArgumentException("invalid order id"));
    }

    @CircuitBreaker(name = "inventoryservice", fallbackMethod = "fallBack")
    public Order saveOrder(Order order){
        Order savedOrder = this.orderRepository.save(order);
        //update the inventory service

        OrderEvent orderEvent = OrderEvent.builder().orderStatus(OrderStatus.ORDER_ACCPETED).orderTimeStamp(LocalDateTime.now()).order(savedOrder).build();
        Message<OrderEvent> orderEventMessage = MessageBuilder.withPayload(orderEvent).build();
        this.source.output().send(orderEventMessage);
        return savedOrder;
    }

    private Order fallBack(Throwable exception){
        log.error("exception while invoking the inventory service :: {}", exception.getMessage());
        return Order.builder().build();
    }

    public void updateOrder(long orderId, Order order){
        this.orderRepository
                .findById(orderId)
                .ifPresent(fetchedOrder -> {
                    Order tobeSavedOrder = Order.builder()
                            .price(order.getPrice())
                            .id(fetchedOrder.getId())
                            .customerName(order.getCustomerName())
                            .time(order.getTime())
                            .build();
                    tobeSavedOrder.setLineItems(order.getLineItems());
                    order.getLineItems().forEach(lineItem -> lineItem.setOrder(tobeSavedOrder));
                    this.orderRepository.save(tobeSavedOrder);
                });
    }

    public void deleteOrderById(long id){
        this.orderRepository.deleteById(id);
    }
}
