package com.classpath.ordermicroservice.config;

import com.classpath.ordermicroservice.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
class DBHealthCheckIndicator implements HealthIndicator{
    private final OrderRepository orderRepository;

    @Override
    public Health health() {
        try {
            long count = this.orderRepository.count();
            return Health.up().withDetail("DB-status","DB-Health is up").build();
        } catch (Exception exception){
            log.error("Exception while connecting to the DB:: {}", exception.getMessage());
            return Health.down().withDetail("DB-status","DB-Health is down").build();
        }
    }
}

@Component
class KafkaHealthCheckIndicator implements HealthIndicator{

    @Override
    public Health health() {
        return Health.up().withDetail("Kafka-status","Kafka-Health is up").build();
    }
}

@Component
class PaymentGatewayHealthCheckIndicator implements HealthIndicator{

    @Override
    public Health health() {
        return Health.up().withDetail("Payment-status","Payment-Health is up").build();
    }
}
