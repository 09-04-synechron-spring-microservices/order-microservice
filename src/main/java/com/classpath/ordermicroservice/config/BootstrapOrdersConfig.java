package com.classpath.ordermicroservice.config;

import com.classpath.ordermicroservice.model.LineItem;
import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@Configuration
@RequiredArgsConstructor
@Slf4j
public class BootstrapOrdersConfig {

    private final OrderRepository orderRepository;
    @Value("${app.orders.count}")
    private int ordersCount;

    private final Faker faker = new Faker();

    @EventListener
    public void bootstrapOrders(ApplicationReadyEvent readyEvent){
      log.info("=========== Application is initialized - start ===========");
        IntStream.range(0, ordersCount)
                .forEach(index -> {
                    Order order = Order.builder()
                                        .customerName(faker.name().firstName())
                                        .time(faker.date().past(4, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
                                        .build();
                    IntStream.range(1, faker.number().numberBetween(2, 4))
                            .forEach(value -> {
                                LineItem lineItem = LineItem.builder()
                                                            .name(faker.commerce().productName())
                                                            .price(faker.number().randomDouble(2, 600, 1200))
                                                            .qty(faker.number().numberBetween(2, 6))
                                                            .build();
                                order.addLineItem(lineItem);
                                order.setPrice(lineItem.getPrice() * lineItem.getQty());

                            });
                    this.orderRepository.save(order);
                });

      log.info("=========== Application is initialized - end ===========");
    }

}
